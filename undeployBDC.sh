#!/bin/bash

usage() {
	echo $0 -u username -t token -a account -f fileToRead -e environmentName
}

chkPackagedComponent() {
	_URL="PackagedComponent/query"
	URL=$baseURL$_URL
	reqTmpl=packageReq.xml.tmpl

	cp tmpl/$reqTmpl tmp/pkgReq.xml
	sed -i "s/\$componentType/$2/g" tmp/pkgReq.xml
	sed -i "s/\$componentID/$3/g" tmp/pkgReq.xml
	curl -s -X $method -u "$user:$token" -H "Accept: application/json" -d @tmp/pkgReq.xml $URL > tmp/"$1.json"
	_pkgUUIDAll=$(jq -r '.result[].packageId' tmp/"$1.json")
	_NF=$(echo $_pkgUUIDAll | awk -F " " "{ print NF }")
	for ((_idx=1;_idx<=$_NF;_idx++))
	do
		_pkgUUID=$(echo $_pkgUUIDAll | awk -v _f=$_idx -F " " '{ print $_f }')
		echo $_procName"~"$_compType"~"$_pkgUUID >> tmp/pkgIDs.json
	done

	if [ ! -s "tmp/pkgIDs.json" ]
	then
		echo "No Packaged Components detected. Perhaps the Folder provided to getCompID.sh was wrong?..."
	else
		echo "getDeployedPackages(): Processing $1..."
		getDeployment "$1"
	fi
}

getDeployment() {
	/bin/rm -f tmp/deploymentIDs.json

	_URL="DeployedPackage/query"
	URL=$baseURL$_URL
	reqTmpl=deployedPkgReq.xml.tmpl

	while read _pkgDet; do
		_procName=$(echo $_pkgDet | awk -F "~" '{ print $1 }')
		_compType=$(echo $_pkgDet | awk -F "~" '{ print $2 }')
		_pkgID=$(echo $_pkgDet | awk -F "~" '{ print $3 }')
		#echo "get_deploy(): Processing $_procName..."
		cp tmpl/$reqTmpl tmp/deployedPkgReq.xml
		sed -i "s/\$componentType/${_compType}/g" tmp/deployedPkgReq.xml
		sed -i "s/\$pkgID/${_pkgID}/g" tmp/deployedPkgReq.xml
		sed -i "s/\$envID/${envUUID}/g" tmp/deployedPkgReq.xml
		curl -s -X $method -u "$user:$token" -H "Accept: application/json" -d @tmp/deployedPkgReq.xml $URL > tmp/"$_procName".Deployment.json
		_deployUUID=$(jq -r '.result[] | select(.active==true)|.deploymentId' tmp/"$_procName".Deployment.json)
		if [ ! -z "$_deployUUID" ]
		then
			echo $_deployUUID >> tmp/deploymentIDs.json
		fi
	done < tmp/pkgIDs.json

	if [ ! -s "tmp/deploymentIDs.json" ]
	then
		echo "No Deployed Packages detected. Please check Packages are deployed to '$env'..."
	else
		echo "doUndeploy(): Processing $1..."
		doUndeploy
	fi
}

doUndeploy() {
	_URL="DeployedPackage/"
	URL=$baseURL$_URL
	method="DELETE"

	while read _deploymentID
	do
		_procName=$(echo $_pkgDet | awk -F "~" '{ print $1 }')
		_URL=$URL$_deploymentID
		echo "undeploy(): Processing $_deploymentID..."
		curl -s -X $method -u "$user:$token" -H "Accept: application/json" $_URL
	done < tmp/deploymentIDs.json
}

spin()
{
	spinner="/|\\-/|\\-"
	while :
	do
		for i in `seq 0 7`
		do
			echo -n "${spinner:$i:1}"
			echo -en "\010"
			sleep 0.5
		done
	done
}

if [ "$#" -ne 10 ]
then
	usage
 	exit 1
fi

while getopts "u:t:a:f:e:" opt; do
	case ${opt} in
		u ) user="BOOMI_TOKEN.${OPTARG}";;
		t ) token=${OPTARG};;
		a ) acct=${OPTARG};;
		f ) BDCFile=${OPTARG};;
		e ) env=${OPTARG};;
		\? ) usage;;
	esac
done

if [ ! -r $BDCFile ]
then
	echo "Unable to read $BDCFile!"
	exit 1
fi

baseURL="https://api.boomi.com/api/rest/v1/$acct/"

#get Environment UUID
reqFile="envReq.xml.tmpl"
cp tmpl/$reqFile tmp/envReq.xml
sed -i "s/\$envName/${env}/g" tmp/envReq.xml

_URL="Environment/query"
URL=$baseURL$_URL
method="POST"

curl -s -X $method -u "$user:$token" -H "Accept: application/json" -d @tmp/envReq.xml $URL > tmp/env.json
envUUID=$(jq -r '.result[0].id' tmp/env.json)

#check Deployments
spin &
SPIN_PID=$!
trap "kill -9 $SPIN_PID" `seq 0 15`


_procIdx=0
_recs=$(wc -l < $BDCFile)
while read _proc; do
	_procName=$(echo $_proc | awk -F "~" '{ print $1 }')
	_compType=$(echo $_proc | awk -F "~" '{ print $2 }')
	_uuid=$(echo $_proc | awk -F "~" '{ print $3 }')
	if [ ! -z "$_procName" -a ! -z "$_compType" -a ! -z "$_uuid" ]
	then
		_procIdx=$((_procIdx+1))
		echo "qryPackagedComponents(): Processing $_procName..."
		/bin/rm -f tmp/pkgIDs.json
		method="POST"
		chkPackagedComponent "$_procName" "$_compType" "$_uuid"
		_perc=$(gawk -vn=$_procIdx -vt=$_recs 'BEGIN{printf("%3.2f\n",n/t*100)}')
		echo $_perc"%..."
	fi
done < $BDCFile

kill -9 $SPIN_PID
echo "Completed"
