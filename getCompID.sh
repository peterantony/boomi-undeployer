#!/bin/bash

usage()
{
	echo Usage: $0 -u username -t token -a account -f objectFile -b boomiFolder
}

spin()
{
	spinner="/|\\-/|\\-"
	while :
	do
		for i in `seq 0 7`
		do
			echo -n "${spinner:$i:1}"
			echo -en "\010"
			sleep 0.5
		done
	done
}

if [ "$#" -ne 10 ]
then
	usage
	exit 1
fi

while getopts "u:t:a:f:b:" opt; do
	case ${opt} in
		u) user="BOOMI_TOKEN.${OPTARG}";;
		t) token=${OPTARG};;
		a) acct=${OPTARG};;
		f) objFile=${OPTARG};;
		b) boomiFolderName=${OPTARG};;
		\?) usage;;
	esac
done

reqTmpl=compDetReq.xml.tmpl
method="POST"

spin &
# Make a note of its Process ID (PID):
SPIN_PID=$!
# Kill the spinner on any signal, including our own exit.
trap "kill -9 $SPIN_PID" `seq 0 15`

baseURL="https://api.boomi.com/api/rest/v1/$acct"
URL="ComponentMetadata/query"
URLReq="$baseURL/$URL"

/bin/rm -f BDC.mf

if [[ ! -f $objFile && ! -r $objFile ]]
then
	echo "Unable to read $objFile"
	exit 10
fi

_idx=0
_recs=$(wc -l < $objFile)
while read _inCompName
do
	if [ ! -z "$_inCompName" ]
	then
		_idx=$((_idx+1))
		_perc=$(gawk -vn=$_idx -vt=$_recs 'BEGIN{printf("%3.2f\n",n/t*100)}')
		#echo "Processing $_inCompName..."
		cp tmpl/$reqTmpl tmp/compDetReq.xml
		sed -i "s/\$compName/$_inCompName/g" tmp/compDetReq.xml
		curl -s -X $method -u "$user:$token" -H "Accept: application/json" -d @tmp/compDetReq.xml $URLReq > tmp/"$_inCompName".json
		_compID=$(jq -r --arg boomiFolder "$boomiFolderName" '.result[] | select(.folderName==$boomiFolder and (.type=="process" or .type=="webservice" or .type=="flowservice") and .currentVersion==true and .deleted==false)|.componentId' tmp/"$_inCompName".json)
		_compType=$(jq -r --arg boomiFolder "$boomiFolderName" '.result[] | select(.folderName==$boomiFolder and (.type=="process" or .type=="webservice" or .type=="flowservice") and .currentVersion==true and .deleted==false)|.type' tmp/"$_inCompName".json)
		echo $_inCompName"~"$_compType"~"$_compID >> BDC.mf
		echo -n $_perc"%..."
	fi
done < $objFile

kill -9 $SPIN_PID
echo "Completed"
