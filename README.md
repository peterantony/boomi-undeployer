# Boomi Undeployer

## Undeploys Boomi Components from an Environment

Run by
1. creating a Text file containing process names
2. run `getCompID.sh`
3. run `undeploy.sh`

### Prerequisites
Make sure you also copy the `tmpl` directory with the AtomSphere Request template files

### HowTo run getCompID
`getCompID.sh -u boomiUsername -t token -a boomiAccount -f objectFilename -b boomiFolderName`<br>All going well, you should see a file named `BDC.mf` containing Process Name and uuid

### HowTo run undeploy
`undeploy.sh -u boomiUsername -t token -a boomiAccount -f objectManifestFilename -e boomiEnvironment`
